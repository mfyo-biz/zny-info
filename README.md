[![GitHub issues](https://img.shields.io/github/issues/mfyo-biz/zny-info.svg?style=plastic)](https://github.com/mfyo-biz/zny-info/issues)
[![GitHub forks](https://img.shields.io/github/forks/mfyo-biz/zny-info.svg?style=plastic)](https://github.com/mfyo-biz/zny-info/network)
[![GitHub license](https://img.shields.io/github/license/mfyo-biz/zny-info.svg?style=plastic)](https://github.com/mfyo-biz/zny-info/blob/master/LICENSE)
    

# ぜにいんふぉ
BitZeny WP Payの後を次ぐプラグイン。
Pay機能は廃止したWebでの表示に特化する    
# 開発方針  
方針(目標)としては初心者でもじゃない人でも簡単に確認でき、カスタマイズできるWebアプリにしていく  
wikiみたいに簡単に確認できるWebアプリにしていく  
# 開発言語  
PHP 7.2.18  
Uikit
Vine.js
# 推奨ブラウザ  
Firefox Chromeなどのモダンブラウザの最新版  
(Internet Explorerなどのレガシーブラウザでは利用できません)  
# ライセンス  
MIT License  
  
# 作者  
Yocchan1513 Team (Yocchan1513)  
  
