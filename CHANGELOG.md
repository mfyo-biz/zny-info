# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- 残高表示の追加  
- iOS機能「Wallet」機能実装 (QRコード)  
- 各コンテンツを移動できる機能  
- 各コンテンツを表示/非表示できる機能  
- Android iOSアプリの制作、公開  (iOSに関しては審査が通らないと思う)  
## Changed  
- ぜにいんふぉの軽量化

  ## [1.2.1] - 2019-09-13  
### Changed
- ユーザー名表示 ログアウトボタンなどの修正 

## [1.2.0] - 2019-08-29  
### Added  
- BitZenyマイニングプールの稼働状態の表示  
  
## [1.1.2] - 2019-08-28  
### Added  
-   CSSフレームワーク Materializeを追加し一部変更  
-   スマホ向けのQRコード生成機能、QRリーダー機能  
### Changed  
-   URL変更 (https://app.zny-webinfo/site)  

### Removed  
- ログアウトページを廃止 (ログアウトページを経由しなくてもログアウトできるようになったため)  

## [1.1.1-logo] - 2019-08-21  
### Added
ぜにいんふぉのロゴを掲載  

## [1.1.1] - 2019-08-17  
### Changed  
-   BitZeny価格表示URL変更に伴う変更とアイコンを追加 (https://zny-info.web.app/)  

### Removed  
- ログアウトページを廃止 (ログアウトページを経由しなくてもログアウトできるようになったため)  


## [1.1.0] - 2019-08-13  
### Changed  
-   プロジェクトの基本カラーをわかみどり色に変更  
### Removed  
- ログアウトページを廃止 (ログアウトページを経由しなくてもログアウトできるようになったため)  

## [1.0.1-1] - 2019-08-12  
### Changed  
-   0x3939bさんのプルリクエストによるコードの変更 [プルリクエスト内容](https://github.com/yocchan1513-team/zny-info/pull/21)

## [1.0.1] - 2019-08-12  
### Changed  
- BitZeny価格表示を修正 
- BitZeny価格表示提供URL変更に伴う変更
  
## [1.0.0] - 2019-08-08  
### Changed  
- BitZeny Web Informationからぜにいんふぉへ変更  
